# mcwebui

![](https://img.shields.io/badge/written%20in-PHP-blue)

Two web interfaces for the game Minecraft.

mcwebui1 features:
- Display chat history and online player list by log parsing
- Semi-realtime ajax updating (polling)
- Allow users to download world backups

mcwebui2 features:
- Connect to Minecraft server using rcon
- Display online player list
- Change the time of day
- Post global messages
- Switch users between survival and classic (flying)
- Teleport and manage teleport requests
- Multiple user logins, permission system with cross-user granting, change password etc.


## Download

- [⬇️ mcwebui-1.0a.rar](dist-archive/mcwebui-1.0a.rar) *(46.40 KiB)*
